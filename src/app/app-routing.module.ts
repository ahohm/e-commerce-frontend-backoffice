import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {UsersComponent} from './users/users.component'
import {OrdersComponent} from './orders/orders.component'
import {OrdersDelevredComponent} from './orders-delevred/orders-delevred.component'
import {ProductsComponent} from './products/products.component'
import {ProductsPromotionComponent} from './products-promotion/products-promotion.component'
import {ReportsComponent} from './reports/reports.component'
import {SellersComponent} from './sellers/sellers.component'
import {LoginComponent} from './login/login.component'
import {CategoryComponent} from './category/category.component'

const routes: Routes = [
  {path:'users-list', component: UsersComponent},
  {path:'orders-list', component: OrdersComponent},
  {path:'delivred-orders-list', component: OrdersDelevredComponent},
  {path:'products-list', component: ProductsComponent},
  {path:'products-promotion-list', component: ProductsPromotionComponent},
  {path:'category', component: CategoryComponent},
  {path:'reports-list', component: ReportsComponent},
  {path:'sellers-list', component: SellersComponent},
  {path:'login', component: LoginComponent},
  {path:'**', redirectTo:''}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
