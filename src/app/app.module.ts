import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { AuthGuardService } from './services/auth-guard.service';
import { DataService } from './services/data.service';
import { RestApiService } from './services/rest-api.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { SellersComponent } from './sellers/sellers.component';
import { ProductsComponent } from './products/products.component';
import { ProductsPromotionComponent } from './products-promotion/products-promotion.component';
import { OrdersComponent } from './orders/orders.component';
import { OrdersDelevredComponent } from './orders-delevred/orders-delevred.component';
import { ReportsComponent } from './reports/reports.component';
import { LoginComponent } from './login/login.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FooterComponent } from './footer/footer.component';
import { CategoryComponent } from './category/category.component';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    SellersComponent,
    ProductsComponent,
    ProductsPromotionComponent,
    OrdersComponent,
    OrdersDelevredComponent,
    ReportsComponent,
    LoginComponent,
    NavbarComponent,
    SidebarComponent,
    FooterComponent,
    CategoryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule

  ],
  providers: [AuthGuardService, DataService, RestApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
