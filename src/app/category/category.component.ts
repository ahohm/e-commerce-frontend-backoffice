import { async } from '@angular/core/testing';
import { RestApiService } from '../services/rest-api.service';
import { DataService  } from '../services/data.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  result:any;
  name:any;
  constructor(private data: DataService,
    private rest: RestApiService,
    private router: Router,) { }

  async ngOnInit() {
    try {
      const data = await this.rest.get(`http://localhost:5000/api/admin/allcategories`);
      data['success'] ? this.result = data['categories'] : this.data.error(data['message']);
    } catch (error) {
      this.data.error(error['message']);

    } 
  }

  async delete(x){
    try{
       await this.rest.delete(`http://localhost:5000/api/admin/categories/`+x);
    } catch(err){
      this.data.error(err['message']);
    }
  }
  async save(){
    try{
       await this.rest.post(`http://localhost:5000/api/categories`,{
         name : this.name
       });
    } catch(err){
      this.data.error(err['message']);
    }
  }
}
