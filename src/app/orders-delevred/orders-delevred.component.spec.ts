import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdersDelevredComponent } from './orders-delevred.component';

describe('OrdersDelevredComponent', () => {
  let component: OrdersDelevredComponent;
  let fixture: ComponentFixture<OrdersDelevredComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdersDelevredComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersDelevredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
