import { async } from '@angular/core/testing';
import { RestApiService } from '../services/rest-api.service';
import { DataService  } from '../services/data.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-orders-delevred',
  templateUrl: './orders-delevred.component.html',
  styleUrls: ['./orders-delevred.component.scss']
})
export class OrdersDelevredComponent implements OnInit {

  result:any;
  constructor(private data: DataService,
    private rest: RestApiService,
    private router: Router,) { }

    async ngOnInit() {
    try {
      const data = await this.rest.get(`http://localhost:5000/api/admin/waitingState`);
      data['success'] ? this.result = data['orders'] : this.data.error(data['message']);
    } catch (error) {
      this.data.error(error['message']);

    }  
  }


}
