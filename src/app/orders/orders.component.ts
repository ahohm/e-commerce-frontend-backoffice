import { async } from '@angular/core/testing';
import { RestApiService } from '../services/rest-api.service';
import { DataService  } from '../services/data.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  result:any;
  constructor(private data: DataService,
    private rest: RestApiService,
    private router: Router,) { }

    async ngOnInit() {
    try {
      const data = await this.rest.get(`http://localhost:5000/api/admin/orders`);
      data['success'] ? this.result = data['orders'] : this.data.error(data['message']);
    } catch (error) {
      this.data.error(error['message']);

    }  
  }

  async changeState(id){
    try{
      await this.rest.get(`http://localhost:5000/api/admin/state/`+id);
    } catch(err){
      this.data.error(err['message']);
    }
  }
}

