import { RestApiService } from '../services/rest-api.service';
import { DataService  } from '../services/data.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-products-promotion',
  templateUrl: './products-promotion.component.html',
  styleUrls: ['./products-promotion.component.scss']
})
export class ProductsPromotionComponent implements OnInit {

  products: any;
  content: any;
  result: any;
  date: any;
  btnDisabled = false;

  constructor(
    private data: DataService,
    private rest: RestApiService,
    private router: Router,
  ) {}

  async ngOnInit() {
    try {
      const data = await this.rest.get(`http://localhost:5000/api/admin/allproducts-promotion`);
      data['success'] ? this.result = data['products'] : this.data.error(data['message']);
      this.date = Date.now()
    } catch (error) {
      this.data.error(error['message']);

    }  
  }



  }
