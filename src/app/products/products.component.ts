import { RestApiService } from '../services/rest-api.service';
import { DataService  } from '../services/data.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  products: any;
  content: any;
  result: any;

  btnDisabled = false;

  constructor(
    private data: DataService,
    private rest: RestApiService,
    private router: Router,
  ) {}

  async ngOnInit() {
    try {
      const data = await this.rest.get(`http://localhost:5000/api/admin/allproducts`);
      data['success'] ? this.result = data['products'] : this.data.error(data['message']);
    } catch (error) {
      this.data.error(error['message']);

    }  
  }

  isPromo(x){
    if(Date.parse(x)>Date.now()){
      return true
    }else{
      return false
    }
  }


  async getUsers() {
    this.content = null;
    try {
      const data = await this.rest.get(`http://localhost:5000/api/accounts/sellers`);
      data['success'] ? this.result = data['users'] : this.data.error(data['message']);
    } catch (error) {
      this.data.error(error['message']);

    }
  }

}
