import { async } from '@angular/core/testing';
import { RestApiService } from '../services/rest-api.service';
import { DataService  } from '../services/data.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {
  result:any;
  constructor(private data: DataService,
    private rest: RestApiService,
    private router: Router,) { }

  async ngOnInit() {
    try {
      const data = await this.rest.get(`http://localhost:5000/api/admin/reportedReviews`);
      data['success'] ? this.result = data['reviews'] : this.data.error(data['message']);
    } catch (error) {
      this.data.error(error['message']);

    }  
  }

  async delete(x){
    try{
       await this.rest.delete(`http://localhost:5000/api/admin/review/`+x);
    } catch(err){
      this.data.error(err['message']);
    }
  }

}
