import { RestApiService } from '../services/rest-api.service';
import { DataService  } from '../services/data.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-sellers',
  templateUrl: './sellers.component.html',
  styleUrls: ['./sellers.component.scss']
})
export class SellersComponent implements OnInit {

  users: any;
  content: any;
  result: any;

  btnDisabled = false;

  constructor(
    private data: DataService,
    private rest: RestApiService,
    private router: Router,
  ) {}

  async ngOnInit() {
    try {
      const data = await this.rest.get(`http://localhost:5000/api/admin/sellers`);
      data['success'] ? this.result = data['sellers'] : this.data.error(data['message']);
    } catch (error) {
      this.data.error(error['message']);

    }  
  }


  async getUsers() {
    this.content = null;
    try {
      const data = await this.rest.get(`http://localhost:5000/api/accounts/sellers`);
      data['success'] ? this.result = data['users'] : this.data.error(data['message']);
    } catch (error) {
      this.data.error(error['message']);

    }
  }

  

}
