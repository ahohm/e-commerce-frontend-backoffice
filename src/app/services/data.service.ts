import { Injectable } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';

import { RestApiService } from './rest-api.service';
@Injectable({
  providedIn: 'root'
})
export class DataService {

  message = '';
  messageType = 'danger';

  user: any;
  cartItems = 0;

  constructor(private router: Router, private rest: RestApiService) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.message = '';
      }
    });
  }

  error(message) {
    this.messageType = 'danger';
    this.message = message;
  }

  success(message) {
    this.messageType = 'success';
    this.message = message;
  }

  warning(message) {
    this.messageType = 'warning';
    this.message = message;
  }

  // async getUsers() {
  //   try {
      
  //       const data = await this.rest.get(
  //         'http://localhost:5000/api/users',
  //       );
  //       this.user = data['user'];
  //       console.log(this.user);
      
  //   } catch (e) {
  //     this.error(e);
  //   }
  // }

 
}
