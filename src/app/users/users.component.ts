import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../services/data.service';
import { RestApiService } from '../services/rest-api.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  users: any;
  content: any;
  result: any;

  btnDisabled = false;

  constructor(
    private data: DataService,
    private rest: RestApiService,
    private router: Router,
  ) {}

  async ngOnInit() {
    try {
      const data = await this.rest.get(`http://localhost:5000/api/admin/users`);
      data['success'] ? this.result = data['users'] : this.data.error(data['message']);
    } catch (error) {
      this.data.error(error['message']);

    }  
  }


  async getUsers() {
    this.content = null;
    try {
      const data = await this.rest.get(`http://localhost:5000/api/admin/users`);
      data['success'] ? this.result = data['users'] : this.data.error(data['message']);
    } catch (error) {
      this.data.error(error['message']);

    }
  }
  async delete(id){
    try{
      await this.rest.delete(`http://localhost:5000/api/admin/delete/`+id);
    } catch(err){
      this.data.error(err['message']);
    }
  }


}
